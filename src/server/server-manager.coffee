http =  require('follow-redirects').https;
sleep = require('sleep')
fs = require('fs')

readAuthKey = () ->
    filePath = (process.env.HOME || process.env.USERPROFILE) + '/.spawn-instances/authKey'
    fs.readFileSync filePath, 
        encoding : 'utf8'
        
exports.listKeys = (callback) ->
    authKey = readAuthKey()
    headers = 
        "Content-Type" : "application/json"
        Authorization : "Bearer #{authKey}"
    
    option = 
        host : "api.digitalocean.com"
        path : "/v2/account/keys"
        method : "GET"
        headers : headers
        
    request = http.request option, (response) ->
        data = ""
        response.setEncoding('utf8');
        response.on 'data', (chunk) ->
            data += chunk
        response.on 'end', () ->
            callback JSON.parse data
        
    request.end()
        
exports.listImages = (callback) ->
    authKey = readAuthKey()
    headers = 
        "Content-Type" : "application/json"
        Authorization : "Bearer #{authKey}"
    
    option = 
        host : "api.digitalocean.com"
        path : "/v2/images?private=true"
        method : "GET"
        headers : headers
        
    request = http.request option, (response) ->
        data = ""
        response.setEncoding('utf8');
        response.on 'data', (chunk) ->
            data += chunk
        response.on 'end', () ->
            callback JSON.parse data
        
    request.end()
        
exports.createNode = (name, nodeIndex, imageID, keyID, callback) ->
    authKey = readAuthKey()
    body = 
        name : name
        region : "nyc3"
        size : "1gb"
        image : imageID
        ssh_keys : [keyID]
        backups : false
        ipv6 : true
        user_data : null
        private_networking : null

    headers = 
        "Content-Type" : "application/json"
        Authorization : "Bearer #{authKey}"
    
    option = 
        host : "api.digitalocean.com"
        path : "/v2/droplets"
        method : "POST"
        headers : headers
        
    request = http.request option, (response) ->
        data = ""
        response.setEncoding('utf8');
        response.on 'data', (chunk) ->
            data += chunk
        response.on 'end', () ->
            console.log JSON.parse data
            callback nodeIndex, JSON.parse data
        
    bodyString = JSON.stringify body
    
    console.log bodyString
    
    request.write bodyString
    request.end()
    

exports.deleteNode = (id, callback) ->
    authKey = readAuthKey()
    headers = 
        "Content-Type" : "application/json"
        Authorization : "Bearer #{authKey}"
    
    option = 
        host : "api.digitalocean.com"
        path : "/v2/droplets/" + id
        method : "DELETE"
        headers : headers
        
    request = http.request option, (response) ->
        data = ""
        response.setEncoding('utf8');
        response.on 'data', (chunk) ->
            data += chunk
        response.on 'end', () ->
            callback JSON.parse data
        
    request.end()
    

exports.getServerDetails = (id, nodeIndex, callback) ->
    authKey = readAuthKey()
    headers = 
        "Content-Type" : "application/json"
        Authorization : "Bearer #{authKey}"
    
    option = 
        host : "api.digitalocean.com"
        path : "/v2/droplets/" + id
        method : "GET"
        headers : headers
        
    request = http.request option, (response) ->
        data = ""
        response.setEncoding('utf8');
        response.on 'data', (chunk) ->
            data += chunk
        response.on 'end', () ->
            callback nodeIndex, JSON.parse data
        
    request.end()
    
exports.listAllDroplets = (callback) ->
    authKey = readAuthKey()
    headers = 
        "Content-Type" : "application/json"
        Authorization : "Bearer #{authKey}"
    
    option = 
        host : "api.digitalocean.com"
        path : "/v2/droplets/"
        method : "GET"
        headers : headers
        
    request = http.request option, (response) ->
        data = ""
        response.setEncoding('utf8');
        response.on 'data', (chunk) ->
            data += chunk
        response.on 'end', () ->
            callback JSON.parse data
        
    request.end()
    
exports.waitForDropletCreation = (id, nodeIndex, callback) ->
    exports.getServerDetails id, nodeIndex, (nodeIndex, data) ->
        if data.droplet.status != "new" 
            callback nodeIndex, data
        else
            sleep.sleep(20)
            exports.waitForDropletCreation id, nodeIndex, callback