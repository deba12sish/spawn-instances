sv = require './server/server-manager'
sm = require './ssh/session-manager'
fs = require 'fs'
readline = require 'linebyline'


exports.main = (prefix) ->
    sv.listAllDroplets (data) ->
        console.log s for s in data.droplets.filter (d) ->
            (d.name.substring 0, prefix.length) == prefix
        .sort (a,b) ->
            (a.name.substring prefix.length).localeCompare (b.name.substring  prefix.length)
        .map (d) ->
            d.networks.v4[0].ip_address
        