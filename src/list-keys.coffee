sv = require './server/server-manager'
sm = require './ssh/session-manager'
fs = require 'fs'
readline = require 'linebyline'


exports.main = () ->
    sv.listKeys (data) ->
        console.log s for s in data.ssh_keys
        .map (d) ->
            d.id + " :: " + d.name
        