fs = require 'fs'
sshclient = require 'sshclient'

exports.runCommandInServer = (host, port, username, commands, keyFile, callback) ->
    
    opts = 
        host: host
        port: port
        username: username
        privateKey: fs.readFileSync(keyFile)
        readyTimeout: 99999

        debug: true # optional
        console: 
            log :  ->
                out = arguments[4] if 0 == arguments[0].indexOf 'command:'
                err = arguments[3] if 0 == arguments[0].indexOf 'command:'
                callback out, err if callback?
        session: (commands).map (command) ->
            op: 'exec'
            command: command
            

    cb = (err) -> callback(err)
    sshclient.session opts, cb