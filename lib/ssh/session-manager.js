(function() {
  var fs, sshclient;

  fs = require('fs');

  sshclient = require('sshclient');

  exports.runCommandInServer = function(host, port, username, commands, keyFile, callback) {
    var cb, opts;
    opts = {
      host: host,
      port: port,
      username: username,
      privateKey: fs.readFileSync(keyFile),
      readyTimeout: 99999,
      debug: true,
      console: {
        log: function() {
          var err, out;
          if (0 === arguments[0].indexOf('command:')) {
            out = arguments[4];
          }
          if (0 === arguments[0].indexOf('command:')) {
            err = arguments[3];
          }
          if (callback != null) {
            return callback(out, err);
          }
        }
      },
      session: commands.map(function(command) {
        return {
          op: 'exec',
          command: command
        };
      })
    };
    cb = function(err) {
      return callback(err);
    };
    return sshclient.session(opts, cb);
  };

}).call(this);
