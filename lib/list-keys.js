(function() {
  var fs, readline, sm, sv;

  sv = require('./server/server-manager');

  sm = require('./ssh/session-manager');

  fs = require('fs');

  readline = require('linebyline');

  exports.main = function() {
    return sv.listKeys(function(data) {
      var i, len, ref, results, s;
      ref = data.ssh_keys.map(function(d) {
        return d.id + " :: " + d.name;
      });
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        s = ref[i];
        results.push(console.log(s));
      }
      return results;
    });
  };

}).call(this);
