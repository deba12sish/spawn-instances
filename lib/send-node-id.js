(function() {
  var fs, readline, sm, sv;

  sv = require('./server/server-manager');

  sm = require('./ssh/session-manager');

  fs = require('fs');

  readline = require('linebyline');

  exports.main = function(serversConfigFile) {
    var nodeIndex, rl;
    nodeIndex = 0;
    rl = readline(serversConfigFile);
    return rl.on('line', function(line, lineCount, byteCount) {
      return sm.runCommandInServer(line, 22, 'root', ["echo " + (nodeIndex++) + " > HungryHippos/nodeId"], '/home/debasishc/.ssh/id_rsa', function(err) {
        return console.log(err);
      });
    });
  };

}).call(this);
