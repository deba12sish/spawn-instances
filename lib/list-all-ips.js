(function() {
  var fs, readline, sm, sv;

  sv = require('./server/server-manager');

  sm = require('./ssh/session-manager');

  fs = require('fs');

  readline = require('linebyline');

  exports.main = function(prefix) {
    return sv.listAllDroplets(function(data) {
      var i, len, ref, results, s;
      ref = data.droplets.filter(function(d) {
        return (d.name.substring(0, prefix.length)) === prefix;
      }).sort(function(a, b) {
        return (a.name.substring(prefix.length)).localeCompare(b.name.substring(prefix.length));
      }).map(function(d) {
        return d.networks.v4[0].ip_address;
      });
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        s = ref[i];
        results.push(console.log(s));
      }
      return results;
    });
  };

}).call(this);
