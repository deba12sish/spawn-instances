(function() {
  var fs, readline, sm, sv;

  sv = require('./server/server-manager');

  sm = require('./ssh/session-manager');

  fs = require('fs');

  readline = require('linebyline');

  exports.main = function(prefix, clusterName) {
    return sv.listAllDroplets(function(data) {
      var entry, i, len, ref, s;
      String(entry = clusterName + " ");
      ref = data.droplets.filter(function(d) {
        return (d.name.substring(0, prefix.length)) === prefix;
      }).sort(function(a, b) {
        return (a.name.substring(prefix.length)).localeCompare(b.name.substring(prefix.length));
      }).map(function(d) {
        return 'root@' + d.networks.v4[0].ip_address + " ";
      });
      for (i = 0, len = ref.length; i < len; i++) {
        s = ref[i];
        entry += s;
      }
      return console.log(entry);
    });
  };

}).call(this);
