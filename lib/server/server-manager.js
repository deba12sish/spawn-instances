(function() {
  var fs, http, readAuthKey, sleep;

  http = require('follow-redirects').https;

  sleep = require('sleep');

  fs = require('fs');

  readAuthKey = function() {
    var filePath;
    filePath = (process.env.HOME || process.env.USERPROFILE) + '/.spawn-instances/authKey';
    return fs.readFileSync(filePath, {
      encoding: 'utf8'
    });
  };

  exports.listKeys = function(callback) {
    var authKey, headers, option, request;
    authKey = readAuthKey();
    headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + authKey
    };
    option = {
      host: "api.digitalocean.com",
      path: "/v2/account/keys",
      method: "GET",
      headers: headers
    };
    request = http.request(option, function(response) {
      var data;
      data = "";
      response.setEncoding('utf8');
      response.on('data', function(chunk) {
        return data += chunk;
      });
      return response.on('end', function() {
        return callback(JSON.parse(data));
      });
    });
    return request.end();
  };

  exports.listImages = function(callback) {
    var authKey, headers, option, request;
    authKey = readAuthKey();
    headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + authKey
    };
    option = {
      host: "api.digitalocean.com",
      path: "/v2/images?private=true",
      method: "GET",
      headers: headers
    };
    request = http.request(option, function(response) {
      var data;
      data = "";
      response.setEncoding('utf8');
      response.on('data', function(chunk) {
        return data += chunk;
      });
      return response.on('end', function() {
        return callback(JSON.parse(data));
      });
    });
    return request.end();
  };

  exports.createNode = function(name, nodeIndex, imageID, keyID, callback) {
    var authKey, body, bodyString, headers, option, request;
    authKey = readAuthKey();
    body = {
      name: name,
      region: "nyc3",
      size: "1gb",
      image: imageID,
      ssh_keys: [keyID],
      backups: false,
      ipv6: true,
      user_data: null,
      private_networking: null
    };
    headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + authKey
    };
    option = {
      host: "api.digitalocean.com",
      path: "/v2/droplets",
      method: "POST",
      headers: headers
    };
    request = http.request(option, function(response) {
      var data;
      data = "";
      response.setEncoding('utf8');
      response.on('data', function(chunk) {
        return data += chunk;
      });
      return response.on('end', function() {
        console.log(JSON.parse(data));
        return callback(nodeIndex, JSON.parse(data));
      });
    });
    bodyString = JSON.stringify(body);
    console.log(bodyString);
    request.write(bodyString);
    return request.end();
  };

  exports.deleteNode = function(id, callback) {
    var authKey, headers, option, request;
    authKey = readAuthKey();
    headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + authKey
    };
    option = {
      host: "api.digitalocean.com",
      path: "/v2/droplets/" + id,
      method: "DELETE",
      headers: headers
    };
    request = http.request(option, function(response) {
      var data;
      data = "";
      response.setEncoding('utf8');
      response.on('data', function(chunk) {
        return data += chunk;
      });
      return response.on('end', function() {
        return callback(JSON.parse(data));
      });
    });
    return request.end();
  };

  exports.getServerDetails = function(id, nodeIndex, callback) {
    var authKey, headers, option, request;
    authKey = readAuthKey();
    headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + authKey
    };
    option = {
      host: "api.digitalocean.com",
      path: "/v2/droplets/" + id,
      method: "GET",
      headers: headers
    };
    request = http.request(option, function(response) {
      var data;
      data = "";
      response.setEncoding('utf8');
      response.on('data', function(chunk) {
        return data += chunk;
      });
      return response.on('end', function() {
        return callback(nodeIndex, JSON.parse(data));
      });
    });
    return request.end();
  };

  exports.listAllDroplets = function(callback) {
    var authKey, headers, option, request;
    authKey = readAuthKey();
    headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + authKey
    };
    option = {
      host: "api.digitalocean.com",
      path: "/v2/droplets/",
      method: "GET",
      headers: headers
    };
    request = http.request(option, function(response) {
      var data;
      data = "";
      response.setEncoding('utf8');
      response.on('data', function(chunk) {
        return data += chunk;
      });
      return response.on('end', function() {
        return callback(JSON.parse(data));
      });
    });
    return request.end();
  };

  exports.waitForDropletCreation = function(id, nodeIndex, callback) {
    return exports.getServerDetails(id, nodeIndex, function(nodeIndex, data) {
      if (data.droplet.status !== "new") {
        return callback(nodeIndex, data);
      } else {
        sleep.sleep(20);
        return exports.waitForDropletCreation(id, nodeIndex, callback);
      }
    });
  };

}).call(this);
