(function() {
  var fs, sm, sv;

  sv = require('./server/server-manager');

  sm = require('./ssh/session-manager');

  fs = require('fs');

  exports.main = function(prefix, count, imageID, keyID) {
    var fd, fds, i, j, ref, results;
    fd = fs.openSync('servers', 'w');
    fds = fs.openSync('serverIDs', 'w');
    results = [];
    for (i = j = 0, ref = parseInt(count); 0 <= ref ? j < ref : j > ref; i = 0 <= ref ? ++j : --j) {
      results.push(sv.createNode(prefix + i, i, imageID, keyID, function(nodeIndex, response) {
        return sv.waitForDropletCreation(response.droplet.id, nodeIndex, function(nodeIndex, details) {
          fs.writeSync(fd, details.droplet.networks.v4[0].ip_address + '\n');
          return fs.writeSync(fds, details.droplet.id + '\n');
        });
      }));
    }
    return results;
  };

}).call(this);
