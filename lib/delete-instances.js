(function() {
  var fs, readline, sm, sv;

  sv = require('./server/server-manager');

  sm = require('./ssh/session-manager');

  fs = require('fs');

  readline = require('linebyline');

  exports.main = function(idsFile) {
    var nodeIndex, rl;
    nodeIndex = 0;
    rl = readline(idsFile);
    return rl.on('line', function(line, lineCount, byteCount) {
      return sv.deleteNode(line, function(data) {
        return console.log(data);
      });
    });
  };

}).call(this);
