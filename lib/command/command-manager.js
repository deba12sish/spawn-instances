(function() {
  var fs, sm;

  fs = require('fs');

  sm = require('../ssh/session-manager');

  exports.runCommandSet = function(commandSet, callback) {
    var commandConfigFile, commandSetArray, commandSets, commandSetsString;
    commandConfigFile = 'commands.json';
    commandSetsString = fs.readFileSync(commandConfigFile, {
      encoding: 'UTF-8'
    });
    commandSets = JSON.parse(commandSetsString);
    commandSetArray = commandSets[commandSet];
    return commandSetArray.map(function(commandSet) {
      var commands, server;
      server = commandSet.server;
      commands = commandSet.commands;
      return sm.runCommandInServer(server, commands, callback);
    });
  };

}).call(this);
